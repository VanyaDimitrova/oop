<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Boardr - Task organizing system

### 1. Description

In its complete form, _Boardr_ will be a system for creating, updating, organizing, and managing tasks, issues, and notes. The application will be built **using the best practices for object-oriented design** (OOD) and **object-oriented programming** (OOP).

<br>

### 2. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **ESLint**

<br>

### 3. Goals

Today, we will lay down the foundations of this system in a functional way. The **goal** is to design the two main *factory* functions - the `createTask` function and the `createBoard` function. We will replace the factory functions with classes later in the course. During this session, you will exercise:

- Working with objects - creating objects and adding `fields` (state) and `methods` (behavior).
- Working with factory functions that create and return objects.
- Protecting the state using validations.

<br>

### 4. Setup

To get the project up and running, follow these steps:

1. Go inside the `Template` folder.
1. Run `npm install` to restore all dependencies.
1. After that, there are a few scripts you can run:

   - `npm run start` (shorthand: `npm start`) - Runs the `src/main.js` file.
   - `npm run lint` - Lints the code inside the `src` folder using **ESLint**.
   - `npm run test` (shorthand: `npm test` or `npm t`) - Runs the unit tests in the console.
   - `npm run test:browser` - Runs the unit tests in the browser in interactive mode.

<br>

### 5. Getting started

The first step is to read the **whole task description**. This will give you an overall idea what is going on and what is expected of you. The next step is to get familiar with the **provided code**. There are a few key things you can do:

- Get familiar with the overall project - what config files exist, what are their settings, what folders are there and what files are there in those folders.
- The code is usually stored in a `src` folder. 
- You can also check the dependency graph, which will give you an overall idea how different parts of the code are connected, but most importantly, in what order to read the code itself.
- Start with the entry point (usually the `main.js` file) and slowly review the code written inside. Then move on to the imported files, read them and move on to their dependencies and etc.
- You can also find comments, hints or even links to resources you should check out to gain further understanding of a specific part of the code or a new concept that is introduced there.

<br>

### 6. The `factory` module

Factory functions in JavaScript are like any other function, they may have parameters and always return an object with a specific function or domain of functions. Factory functions can be bundled together and the `factory`module in `src/factory` does just that.

There are two factory functions inside the `factory` module:

- `createTask(name, due, status)` - returns a `task` object with specific task properties and functions
- `createBoard()` - returns a `board` object with specific board properties and functions

You are provided with the return types (skeletons) of the factory functions, you just need to add some validation and implementation as described below.

<br>

### 6.1 The `task` object

`createTask` should validate passed arguments and return a `task` object. If any of the arguments passes is not valid, the factory function should throw an error.

The `task` object should have the following properties (state):

- `name` - It should never be **null, undefined or an empty string**
- `due` - The date of the deadline. Think of what validation will be needed here.
- `status` - The task's status should never be **null, undefined or an empty string**, and it should be one of the three `Todo`, `In Progress`, or `Done`.

The `task` object should have the following functions (or methods):

- `reset: function() { /* missing implementation */ }` - It should set the status to `Todo`
- `advance: function() { /* missing implementation */ }` - It should set the status to `In Progress`
- `complete: function() { /* missing implementation */ }` - It should set the status to `Done`

In some cases objects need to be printed or logged, and when the program tries to convert them to strings, you might see the following

```text
[object Object]
```

In order to change that behavior you need to implement the `toString()` method of the `task` object and it should return a more descriptive string in the following format:

```text
Name: Update naming
Status: Todo
Due: 9/4/2020 12:00:00 AM
```

### 6.2 Using the `createTask` function

When you have implemented the `createTask` factory function, you can test it in the `src/main.js` file by creating a few task objects:

```javascript
const task1 = factory.createTask('Validate fields', new Date('2021/09/03'));
const task2 = factory.createTask('Write unit tests', new Date('2021/09/04'));
const task3 = factory.createTask('Remove console.log', new Date('2021/09/05'));
```

Then try to put the tasks in an array and convert it to a string:

```javascript
const tasks = [task1, task2, task3];
console.log(`${tasks}`);
```

Instead of

```json
[object Object],[object Object],[object Object]
```

you should see

```text
Name: Validate fields
Status: Todo
Due: 9/3/2021 12:00:00 AM,Name: Write unit tests
Status: Todo
Due: 9/4/2021 12:00:00 AM,Name: Remove console.log
Status: Todo
Due: 9/5/2021 12:00:00 AM
```

You can make that look better, but we'll get to that later.

<br>

### 6.3 The `board` object

The `createBoard` function doesn't take any parameter, so no initial validation is required. It returns a `board` object that has the following properties (state):"

- `tasks` - an array that will hold all the task objects we add to the board object. It is initialized as an empty array

The `board` object has the following functions (or methods):"

- `add: function(task) { /* missing implementation */ }` - Takes a `task` object and adds it to the `tasks` array. It should throw an error if the passed value of `task` is `null` or `undefined`. It should also throw and error if the task has already been added to the array (compare `task` objects by reference)
- `remove: function(task) { /* missing implementation */ }` - Removes a `task` object from the `tasks` array. It should throw an error if the passed value of `task` is `null` or `undefined`.

To make conversion of the `board` to string more meaningful, you should also implement the `toString()` method. It should return two different string models depending on when there are some tasks in the `tasks` array or not:

#### Empty board

```text
---Task Board---

Tasks:

No tasks at the moment.
```

#### Board with some tasks

```text
---Task Board---

Tasks:

Name: Validate fields
Status: InProgress
Due: 9/3/2021 12:00:00 AM
--------
Name: Write unit tests
Status: Done
Due: 9/4/2021 12:00:00 AM
--------
Name: Remove console.log
Status: Todo
Due: 9/5/2021 12:00:00 AM
```

<br>

### 7. Unit tests

To help you with the validations and implementation of missing functionality you have been provided with unit tests, which test specific results of your code implementation. There are two ways to run the tests:

- `npm run test` (shorthand `npm test` or `npm t`) - This will run the tests in the console
- `npm run test:browser` - This will run the test in interactive mode in the browser where you can control when and how tests are run

Unit test will provide you with feedback on what is working in your code and what is not (and why).

<br>

### 8. Testing the `Board` and the `Task` classes

You can paste the following sample code inside the `main.js` file. Running this code should produce the output shown below. Even if the outo:

```javascript
import factory from './factory/factory.js';

// Print a colorful line on the console
const newline = () => console.log('\n \x1b[35m* * * * *\x1b[37m \n');

const board = factory.createBoard();

const task1 = factory.createTask('Validate fields', new Date('2021/09/03'));
const task2 = factory.createTask('Write unit tests', new Date('2021/09/04'));
const task3 = factory.createTask('Remove console.log', new Date('2021/09/05'));

console.log(board.toString());

newline();

board.add(task1);
board.add(task2);
board.add(task3);

task1.advance();
task2.complete();

console.log(board.toString());

newline();

board.remove(task3);

console.log(task1.toString());

```

Sample output text:

```text
---Task Board---       

Tasks:

No tasks at the moment.

 * * * * *

---Task Board---

Tasks:

Name: Validate fields    
Status: In Progress      
Due: 9/3/2021 12:00:00 AM
--------
Name: Write unit tests   
Status: Done
Due: 9/4/2021 12:00:00 AM
--------
Name: Remove console.log 
Status: Todo
Due: 9/5/2021 12:00:00 AM

 * * * * *

Name: Validate fields
Status: In Progress
Due: 9/3/2021 12:00:00 AM
```
