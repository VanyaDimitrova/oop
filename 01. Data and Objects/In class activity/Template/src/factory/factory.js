// Factory functions in JavaScript are functions that may have parameters and return an object
// https://medium.com/javascript-scene/javascript-factory-functions-with-es6-4d224591a8b1/

const createTask = (name, due, status) => {
  // validate the name

  // validate due

  // validate the status

  return {
    name,
    due,
    status,
    reset: function() {
      // missing implementation
    },
    advance: function() {
      // missing implementation
    },
    complete: function() {
      // missing implementation
    },
    toString: function() {
      // missing implementation
    },
  };

};

const createBoard = () => {

    return {
      tasks: [],
      add: function(task) {
        // missing implementation
      },
      remove: function(task) {
        // missing implementation
      },
      toString: function() {
        // missing implementation
      },
    };
};

export default {
  createTask,
  createBoard,
};
