import { status } from './status.js';

/** The Task class holds all relevant data and behavior a task might have. */
export class Task {
  // One way to indicate that a field should not be touched is to put an underscore before its name.
  // This won't prevent your colleges from directly interacting with it.

  _name;
  _dueDate;
  _status;

  changeName(value) {
  }

  changeDueDate(value) {
  }
  
  reset() {
  }

  advance() {
  }

  complete() {
  }
}